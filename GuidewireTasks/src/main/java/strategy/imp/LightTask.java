package strategy.imp;

import exceptions.CustomInputException;
import menu.MenuTask;
import statics.ConsoleColors;
import strategy.Task;

import java.util.Scanner;

public class LightTask  extends MenuTask implements Task {

    @Override
    public void execute() {
        System.out.println("Enter any number from 0 to 60:");
        try {
            calculateLight(new Scanner(System.in).nextDouble());
        }
        catch (Exception ex){
            System.out.println(ConsoleColors.RED+">> ERROR:Error, please, input correct number"+ConsoleColors.RESET);
        }
    }

    private void calculateLight(double adr) throws CustomInputException {
        System.out.println("i mod 5 = " + adr % 5);
        double mod = adr % 5;
        if(mod<3 && mod>=0)
            getColoredText(ConsoleColors.GREEN, "Green");
        if(mod>=3 && mod<4)
            getColoredText(ConsoleColors.YELLOW, "Yellow");
        if(mod>=4)
            getColoredText(ConsoleColors.RED, "Red");
        menuController(this);
    }

    private void getColoredText(String color, String text){
        System.out.println(color+text+ConsoleColors.RESET);
    }
}
