package strategy.imp;

import menu.MenuTask;
import statics.ConsoleColors;
import strategy.Task;

import java.util.Scanner;


public class RecursionTask extends MenuTask implements Task {
    private int count = 1;
    @Override
    public void execute() {
        try {
            System.out.println("Enter any number:");
            int numberCount;
            Scanner scanner = new Scanner(System.in);
            numberCount = scanner.nextInt();
            System.out.println("Natural numbers till "+numberCount+":");
            recursionMethod(numberCount, count);
            count  = 1;
            menuController(this);
        }
        catch (Exception ex){
            System.out.println(ConsoleColors.RED+">> ERROR:Error, please, input correct number"+ConsoleColors.RESET);
        }
    }


    private int recursionMethod(int number, int count) {
        System.out.println(">>  "+count);
        if(count>=number)
            menuController(this);
        return recursionMethod(number, ++count);
    }
}
