package strategy.imp;

import menu.MenuTask;
import statics.ConsoleColors;
import statics.ParserParams;
import strategy.Task;

import java.util.Map;
import java.util.Scanner;

public class ParserTask extends MenuTask implements Task {

    @Override
    public void execute()  {
        System.out.println("Enter any string :");
        try {
            parseAddress(new Scanner(System.in).nextLine());
        }
        catch (Exception ex){
            System.out.println(ConsoleColors.RED+">> ERROR:Error, please, input correct string"+ConsoleColors.RESET);
        }
    }

    private void parseAddress(String adr) {
        ParserParams params = new ParserParams();
        String input = adr;
        for(Map.Entry<String, String> entry : params.getReplacement().entrySet()) {
             input = input.replaceAll(entry.getKey(), entry.getValue());
        }
        System.out.println(input);
        menuController(this);
    }
}
