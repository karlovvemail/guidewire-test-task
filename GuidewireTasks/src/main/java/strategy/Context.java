package strategy;

public class Context {
    private Task strategy;

    // Constructor
    public Context() {
    }

    // Set new strategy
    public void setStrategy(Task strategy) {
        this.strategy = strategy;
    }

    public void executeStrategy() {
         strategy.execute();
    }
}
