package strategy;

public interface Task {
  void execute() ;
}
