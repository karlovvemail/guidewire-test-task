package menu;

import strategy.Task;

import java.util.Scanner;

public class MenuTask{
    public  void menuController(Task task) {
        boolean exit=false;
        do
        {
            System.out.println("1. Re-run this task");
            System.out.println("2. Exit");
            System.out.println("choose one!");
            System.out.println("Enter your choice (1-2)");
            int num=new Scanner(System.in).nextInt();
            switch(num)
            {
                case 1:
                    task.execute();
                    break;
                case 2:
                    exit=true;
                    new MainMenu().menuController();
                    break;
            }
        }while(!exit);
    }
}
