package menu;

import statics.ConsoleColors;
import strategy.Context;
import strategy.imp.LightTask;
import strategy.imp.ParserTask;
import strategy.imp.RecursionTask;

import java.util.InputMismatchException;
import java.util.Scanner;

public class MainMenu {
    public void menuController()  {
        Context context = new Context();
        boolean exit=false;
        do
        {
            System.out.println("1. Recursion number task");
            System.out.println("2. Parsing and replacing task");
            System.out.println("3. Traffic light task");
            System.out.println("4. Exit");
            System.out.println("Choose one!");
            System.out.println("Enter your choice (1-4)");
            int num = 0;
            try{
                 num = new Scanner(System.in).nextInt();
            }
            catch (InputMismatchException ex){
                System.out.println(ConsoleColors.RED+">> ERROR:Error, please, input correct number"+ConsoleColors.RESET);
            }
            switch(num)
            {
                case 0:
                    menuController();
                    break;
                case 1:
                    context.setStrategy(new RecursionTask());
                    context.executeStrategy( );
                    break;
                case 2:
                    context.setStrategy(new ParserTask());
                    context.executeStrategy( );
                    break;
                case 3:
                    context.setStrategy(new LightTask());
                    context.executeStrategy( );
                    break;
                case 4:
                    exit=true;
                    break;
                default:
                    System.out.println(ConsoleColors.RED+">> ERROR: This menu point not found.\nPlease, input correct number"+ConsoleColors.RESET);
                    menuController();
                    break;
            }
        }while(!exit);
        System.exit(0);
    }
}
