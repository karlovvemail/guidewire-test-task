package exceptions;

public class CustomInputException extends Exception {
    public CustomInputException(String errorMessage) {
        super(errorMessage);
    }
}