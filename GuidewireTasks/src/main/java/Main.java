import exceptions.CustomInputException;
import menu.MainMenu;

public class Main {
    public static void main(String[] args) throws CustomInputException {
        MainMenu mainMenu = new MainMenu();
        mainMenu.menuController();
    }


}
