package statics;

import java.util.HashMap;
import java.util.Map;

public  class ParserParams {
    private Map<String, String> replacement = new HashMap<String, String>();

    public Map<String, String> getReplacement() {
        replacement.put("((\\b(St)\\b)\\.)", "Street");
        replacement.put("(\\b(Str)\\b)", "Street");
        replacement.put("((\\b(Ave)\\b)(?=\\,))", "Avenue");
        return replacement;
    }
}
